
// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався 
// конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. 
// У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, 
// і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, 
// написана в джаваскрипті, через такі правки не переставала працювати.


const tabsMenu = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelectorAll('.tabs-content');


tabsMenu.forEach(item => {
    item.addEventListener('click', () => {

        let content = item.getAttribute('data-tab');
        let currentContent = document.querySelector(content);
        

        tabsMenu.forEach(item => {
            item.classList.remove('active');
        })

        tabsContent.forEach(item => {
            item.classList.remove('tabs-content-active');
        })

        item.classList.add('active');
        currentContent.classList.add('tabs-content-active');
    })
})






